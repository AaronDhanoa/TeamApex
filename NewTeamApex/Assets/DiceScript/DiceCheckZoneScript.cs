using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZoneScript : MonoBehaviour
{
    public GameObject shellPrefab;
    public GameObject playerBack;
    Vector3 diceVelocity;

    void FixedUpdate()
    {
        diceVelocity = DiceScript.diceVelocity; 
    }

    void OnTriggerStay(Collider col)
    {
        if (diceVelocity.x == 0f && diceVelocity.y == 0f && diceVelocity.z == 0f)
        {
            switch (col.gameObject.name)
            {
                case "Side1" :
                    DiceNumberTextScript.diceNumber = 6;
                    Shell();
                    break;
                case "Side2" :
                    DiceNumberTextScript.diceNumber = 5;
                    Shell();
                    break;
                case "Side3" :
                    DiceNumberTextScript.diceNumber = 4;
                    Shell();
                    break;
                case "Side4" :
                    DiceNumberTextScript.diceNumber = 3;
                    Shell();
                    break;
                case "Side5" :
                    DiceNumberTextScript.diceNumber = 2;
                    Shell();
                    break;
                case "Side6" :
                    DiceNumberTextScript.diceNumber = 1;
                    Shell();
                    break;
            }
        }
    }

    void Shell()
    {
        
            GameObject a = Instantiate(shellPrefab) as GameObject;
            a.transform.position = playerBack.transform.position;
            Destroy(gameObject);
            Debug.Log("Power up picked up");
        
    }
}
