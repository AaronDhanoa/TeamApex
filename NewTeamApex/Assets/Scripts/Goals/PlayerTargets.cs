using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargets : MonoBehaviour
{
    
    public ParticleSystem visual;
    public UserInterface MoneyCount;
    public RandomGoal boolVal;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Money1") {
        visual.Play();
        MoneyCount.PlayerMoney(Random.Range(500,700));
        boolVal.TargetHit = true;
        Object.Destroy(gameObject, 0.1f);
        boolVal.Targets.Remove(gameObject);
        //Debug.Log("HIT");
        
        }
    }

}
