using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RandomGoal : MonoBehaviour
{
    public List<GameObject> Targets;
    public bool TargetHit = false;
    private int QT = 0;
    public UserInterface UserInterface;
    public GameObject Winscreen;

    void Start()
    {
        Targets[Random.Range(0,Targets.Count)].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (TargetHit == true)
        {
            
            QT++;
            UserInterface.GoalsReachedValue = QT;

            if (QT == 19)
            {
                QT++;
                TargetHit = false;

                Winscreen.SetActive(true);
                StartCoroutine(levelLoadDelay());
                //Debug.Log("Game Over you win" + QT);
                Targets[Random.Range(0,Targets.Count)].SetActive(false);
                UserInterface.GoalsReachedValue = QT;
                

                
                
            }
            else
            {
                Debug.Log(QT);
                Targets[Random.Range(0,Targets.Count)].SetActive(true);
                TargetHit = false;

            }

        }
    }

    IEnumerator levelLoadDelay()
    {
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene("MainMenu");
    }
}
