using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_PS4
using UnityEngine.PS4;
#endif
public class CarMovement : MonoBehaviour
{

    private const string HORIZONTAL = "Horizontal";
    private const string VERTICAL = "Vertical";

    private float f_horizontalInput;
    private float f_verticalInput;
    private float f_steerAngle;
    private float f_currentbreakForce;
    private float f_currentSteerAngle;
    private bool isBreaking;

    [SerializeField] public float f_motorForce;
    [SerializeField] private float f_breakForce;
    [SerializeField] private float f_maxSteeringAngle;

    [SerializeField] private WheelCollider FrontLeftWheelCollider;
    [SerializeField] private WheelCollider FrontRightWheelCollider;
    [SerializeField] private WheelCollider RearLeftWheelCollider;
    [SerializeField] private WheelCollider RearRightWheelCollider;

    [SerializeField] private Transform t_FrontLeftWheelTransform;
    [SerializeField] private Transform t_FrontRightWheelTransform;
    [SerializeField] private Transform t_RearLeftWheelTransform;
    [SerializeField] private Transform t_RearRightWheelTransform;

    void FixedUpdate()
    {
        GetInput();
        HandleMotor();
        HandleSteering();
        UpdateWheels();
    }
    
    private void GetInput()
    {
        f_horizontalInput = Input.GetAxis(HORIZONTAL);
        f_verticalInput = Input.GetAxis(VERTICAL);
        // isBreaking = Input.GetKey(KeyCode.Space);
    }

    private void HandleMotor()
    {
        FrontLeftWheelCollider.motorTorque = f_verticalInput * f_motorForce;
        FrontRightWheelCollider.motorTorque = f_verticalInput * f_motorForce;
        f_currentbreakForce = isBreaking ? f_breakForce : 0f;
        if (isBreaking)
        {
            ApplyBraking();
        }
    }

    private void ApplyBraking()
    {
        FrontRightWheelCollider.brakeTorque = f_currentbreakForce;
        FrontLeftWheelCollider.brakeTorque = f_currentbreakForce;
        RearLeftWheelCollider.brakeTorque = f_currentbreakForce;
        RearRightWheelCollider.brakeTorque = f_currentbreakForce;
    }

    private void HandleSteering()
    {
        f_currentSteerAngle = f_maxSteeringAngle * f_horizontalInput;
        FrontLeftWheelCollider.steerAngle = f_currentSteerAngle;
        FrontRightWheelCollider.steerAngle = f_currentSteerAngle;
    }

    private void UpdateWheels()
    {
        UpdateSingleWheel(FrontLeftWheelCollider, t_FrontLeftWheelTransform);
        UpdateSingleWheel(FrontRightWheelCollider, t_FrontRightWheelTransform);
        UpdateSingleWheel(RearLeftWheelCollider, t_RearLeftWheelTransform);
        UpdateSingleWheel(RearRightWheelCollider, t_RearRightWheelTransform);
    }

    private void UpdateSingleWheel(WheelCollider WheelCollider, Transform WheelTransform)
    {
        Vector3 pos;
        Quaternion rot;
        WheelCollider.GetWorldPose(out pos, out rot);
        WheelTransform.rotation = rot;
        WheelTransform.position = pos;
    }    
   
   void Update()
   {
       
    //if (Input.GetKeyDown(KeyCode.L) || Input.GetKeyDown(KeyCode.JoystickButton0))
    //{
    //    SceneManager.LoadScene("MainMenu");
    //}

    if (Input.GetKeyDown(KeyCode.JoystickButton3))
    {
        SceneManager.LoadScene("Level1");
    }

   }

}
