using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StreetWalkAI : MonoBehaviour {

	GameObject[] PathLocations;
	UnityEngine.AI.NavMeshAgent navMeshWalkers;


	// Use this for initialization
	void Start () {
		PathLocations = GameObject.FindGameObjectsWithTag("goal");
		navMeshWalkers = this.GetComponent<NavMeshAgent>();
		navMeshWalkers.SetDestination(PathLocations[Random.Range(0,PathLocations.Length)].transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		
		if(navMeshWalkers.remainingDistance < 1)
		{
				navMeshWalkers.SetDestination(PathLocations[Random.Range(0,PathLocations.Length)].transform.position);
		}
	}
}
