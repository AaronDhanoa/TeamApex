using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuScript : MonoBehaviour
{

     public GameObject pauseMenuFButton, optionsMenuFButton, closeOpMenuButton;
    public GameObject mainMenuPanel;
    public GameObject optionsPanel;
    
    void Update()
    {


        if (Input.GetKeyDown(KeyCode.F1) || Input.GetKeyDown(KeyCode.JoystickButton7))
         {
             if (optionsPanel == true)
                {
                mainMenuPanel.SetActive(true);
                optionsPanel.SetActive(false);
                }
         }
    }
    public void play()
    {
        SceneManager.LoadScene("Level1");
    }


    public void LevelSelector()
    {
        SceneManager.LoadScene("LevelSelector");

    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Options()
    {
        mainMenuPanel.SetActive(false);
        optionsPanel.SetActive(true);

        // selected object 
        EventSystem.current.SetSelectedGameObject(null);
        // set new selected object
        EventSystem.current.SetSelectedGameObject(optionsMenuFButton);

    }

    public void MenuBtn()
    {
        if (optionsPanel == true)
        {
            mainMenuPanel.SetActive(true);
            optionsPanel.SetActive(false);

             // selected object 
        EventSystem.current.SetSelectedGameObject(null);
        // set new selected object
        EventSystem.current.SetSelectedGameObject(closeOpMenuButton);
        }
    }

}
