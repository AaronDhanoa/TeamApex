using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GUI : MonoBehaviour
{

   public GameObject pauseMenu;

   public void Unpause()
    {
        if (pauseMenu == true)
        {
            pauseMenu.SetActive(false);
            
        }
    }

    public void pause()
    {
        if (pauseMenu == false)
        {
            pauseMenu.SetActive(true);
        }
    }

    void update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {

            pause();
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    
    public void MainMenuShortcut()
    {
        SceneManager.LoadScene("MainMenu");

    }
}
