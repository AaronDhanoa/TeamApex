using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;

public class UserInterface : MonoBehaviour
{
    public GameObject pauseMenuFButton, optionsMenuFButton, closeOpMenuButton;
    public TextMeshProUGUI moneyUiCount;
    public static bool isPaused = false;
    public GameObject PauseMenuGUI;
    public GameObject PauseMOptions;
    public GameObject LossScreen;
    public int Money;
    static float MoneyCount;
    public int GoalsReachedValue;
    private Slider GoalsReached;
    

    void Start()
    {
        GoalsReached = GameObject.Find("goalsReached").GetComponent<Slider>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1) || Input.GetKeyDown(KeyCode.JoystickButton7))
        {
            if (isPaused)
            {
                Continue();
                PauseMOptions.SetActive(false);

                // selected object 
                EventSystem.current.SetSelectedGameObject(null);
                // set new selected object
                EventSystem.current.SetSelectedGameObject(pauseMenuFButton);
            }
            else
            {
                Paused();
            }
        }

        goalsSlider();
    }


    void Continue()
    {
        PauseMenuGUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;

    }

    void Paused()
    {
        PauseMenuGUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;

    }

    public void ContinueBTN()
    {
        Continue();
    }

    public void OptionsBtn()
    {
        PauseMOptions.SetActive(true);
        PauseMenuGUI.SetActive(false);
        Time.timeScale = 0f;

        // selected object 
        EventSystem.current.SetSelectedGameObject(null);
        // set new selected object
        EventSystem.current.SetSelectedGameObject(optionsMenuFButton);

    }

    public void ReturnBtn()
    {
        PauseMOptions.SetActive(false);
        PauseMenuGUI.SetActive(true);
        Time.timeScale = 0f;

        // selected object 
        EventSystem.current.SetSelectedGameObject(null);
        // set new selected object
        EventSystem.current.SetSelectedGameObject(closeOpMenuButton);
    }

    public void ExitBTN()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1f;
     
    }

    public void PlayerMoney(int Money)
    {
        MoneyCount+= Money;
        //Debug.Log(MoneyCount);
        moneyUiCount.text = "" + MoneyCount;

        if (MoneyCount < -10)
        {
            LossScreen.SetActive(true);

            StartCoroutine(levelLoadDelay());
        }

    }

    public void goalsSlider()
    {
        GoalsReached.value = GoalsReachedValue;

    }

    IEnumerator levelLoadDelay()
    {
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene("MainMenu");
    }

}
