using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

// https://www.youtube.com/watch?v=YOaYQrN1oYQ

public class OptionsMenu : MonoBehaviour
{


    void Update()
    {
        
    }

    public AudioMixer audioMixer;

    public void SetVolume(float decimalVolume)
    {
        var dbVolume = Mathf.Log10(decimalVolume) * 20;
        if (decimalVolume == 0.0f)
        {
        dbVolume = -80.0f;
        }
        audioMixer.SetFloat("volume", dbVolume);
    }

    public void setQuality (int qualityIndex)
    {
    QualitySettings.SetQualityLevel(qualityIndex);
    }   

}
