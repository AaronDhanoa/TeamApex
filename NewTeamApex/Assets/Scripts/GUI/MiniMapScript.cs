using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapScript : MonoBehaviour
{
    // get the player position in the world
    public Transform PlayerVehicle;

    void LateUpdate()
    {
        // specify the players world position to the new position
        Vector3 newPosition = PlayerVehicle.position;
        // get the y position of the player to the new position 
        newPosition.y = transform.position.y;
        // set the vector3 position of the player position
        transform.position = newPosition;
    }
}
