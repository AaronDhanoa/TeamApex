using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyCollectable : MonoBehaviour
{

    public UserInterface MoneyCount;
    
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player") {
        gameObject.tag = "MoneyCollectable";
        MoneyCount.PlayerMoney(Random.Range(100,125));
        Destroy(gameObject, 0.2f);
        }

        
    }

    
}
